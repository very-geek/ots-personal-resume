module.exports = function (grunt) {
  'use strict';

  grunt.config.init({
    copy: {
      images: {
        expand: true,
        cwd: 'source/images',
        src: '**/*.{gif,jpg,jpeg,png}',
        dest: 'public/images/',
        flatten: true
      }
    },
    jade: {
      compile: {
        expand: true,
        cwd: 'source/',
        src: ['**/*.jade', '!**/_*.jade'],
        dest: 'public/',
        ext: '.html',
        options: { pretty: true }
      }
    },
    sass: {
      options: {
        trace: true,
        lineNumbers: true,
        style: 'expanded',
        loadPath: 'source/styles/'
      },
      compile: {
        expand: true,
        cwd: 'source/styles',
        src: ['**/*.scss', '!**/_*.scss'],
        dest: 'public/styles/',
        ext: '.css'
      }
    },
    watch: {
      options: { livereload: true },
      images: {
        files: ['source/images/**.*{gif,jpg,jpeg,png}'],
        tasks: ['copy:images']
      },
      jade: {
        files: ['source/**/*.jade'],
        tasks: ['jade:compile']
      },
      sass: {
        files: ['source/styles/**/*.scss'],
        tasks: ['sass:compile']
      }
    },
    connect: {
      server: {
        options: {
          port: 1234,
          open: 'http://localhost:1234/public',
          livereload: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');

  grunt.registerTask('default', ['connect:server', 'watch']);
};
